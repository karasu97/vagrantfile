#!/bin/sh

echo "Install Firewalld"
systemctl start firewalld && systemctl enable  firewalld

echo "Install Git"
sudo yum -y install gcc curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-ExtUtils-MakeMaker autoconf
sudo yum -y install wget
cd /opt/
sudo wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.9.5.tar.gz
sudo tar xzvf git-2.9.5.tar.gz
sudo rm git-2.9.5.tar.gz
cd git-2.9.5/
sudo make prefix=/usr/local install
git --version