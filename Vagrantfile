require 'yaml'
require 'erb'

# Doc: https://developer.hashicorp.com/vagrant/docs/vagrantfile/machine_settings
# Blog：https://qiita.com/mintak21/items/680b2d8cc1a41d1c4d00

if not File.file?("./config.yml")
  abort("File `config.yml` does not exist.")
end

CONFIG_YML = YAML.load(ERB.new(File.read('config.yml')).result, aliases: true)

def check_config(host_config, require_keys)
  abort("host not defined in config.yml") if host_config.nil?
  require_keys.each do |k|
    abort("#{k} is nil") if host_config.dig(*k).nil?
  end
  return
end

def create_vm(hostname, hostconfig)
  Vagrant.configure("2") do |config|
    config.vm.define hostname do |c|
      # VM
      c.vm.box = hostconfig['os']
      c.vm.hostname = hostname
      c.vm.provision "shell" , privileged: true, path: "./provision.sh"

      # Network
      c.vm.network :private_network, ip: hostconfig['private_ip']
      c.vm.network :forwarded_port, id: "ssh", guest: 22, host: hostconfig['ssh_port_forward']

      # Provider
      c.vm.provider "virtualbox" do |v|
        v.cpus = hostconfig['cpu']
        v.memory = hostconfig['memory_mb']
        vdi_path = "./disk/#{hostname}_sdb.vdi"
        v.customize ["createvdi", "--filename", vdi_path, "--size", 2048]  if not File.exist?(vdi_path)
        v.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--port", 1, "--device", 0, "--type", "hdd", "--medium", vdi_path]
      end
    end
  end
end

for name in  CONFIG_YML['host'].keys do
  config = CONFIG_YML['host'][name]
  # 必須項目が揃っているかを確認
  check_config(config, ['os', 'cpu', 'memory_mb', 'private_ip', 'ssh_port_forward'])
  # 仮想マシンを作成
  create_vm(name, config)
end
