### 使い方

1. https://developer.hashicorp.com/vagrant/downloads より、ローカル端末に合う Vagrant をインストール。
2. `asdf` を用いて Ruby をインストールしたい場合、`asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git`を実行。
3. `asdf install ruby 3.1.4`を実行。
4. `asdf global ruby 3.1.4`を実行。
5. `cp config-template.yml config.yml`を実行。
6. 以下の`config.yml`に作成したい仮想マシンの設定情報を入力。

```config.yml:yml
host:
  node1: &common
    os: "bento/almalinux-9"
    cpu: 1
    memory_mb: 512
    private_ip: 192.168.56.21
    ssh_port_forward: 22221
  node2:
    <<: *common
    private_ip: 192.168.56.22
    ssh_port_forward: 22212
  # 追加したくない仮想マシンには コメントアウト
  # node3:
  #  <<: *common
  #  private_ip: 192.168.56.23
  #  ssh_port_forward: 22223
```

7. 既存ホスト名で作成済の仮想マシンが存在する場合、`vagrant destroy -f [対象ホスト]` を実行。もしくは、`bash rebuild.sh`を実行。
8. `vagrant up [対象ホスト]` を実行し、仮想マシンを作成。
9. `vagrant ssh-config [対象ホスト] >> ~/.ssh/config`を実行し、SSH 設定を追加。
10. `ssh [対象ホスト]`を実行し、仮想マシンへアクセスできれば完了。
